package main

import "fmt"

func main() {
	conn := ConnectToDB()
	defer conn.Close()

	receipt := ""
	receiptDetail := ""
	paymentType := ""

	receiptRes := ParseRowFromDB(receipt, conn)
	receiptDetailRes := ParseRowsFromDB(receiptDetail, conn)
	paymentTypeRes := ParseRowsFromDB(paymentType, conn)

	maps := MergeMaps(receiptRes, receiptDetailRes, paymentTypeRes)
	before := Marshal(maps)

	cmaps := JsonReplacer(maps)
	after := Marshal(cmaps)

	fmt.Println("before: ", string(before))
	fmt.Println("after: ", string(after))
}
