package main

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

const (
	host   = ""
	port   = ""
	user   = ""
	pass   = ""
	dbname = ""
)

func ConnectToDB() *sql.DB {
	psqlconn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, pass, dbname)
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		log.Fatalf("cannot connect to database: %s", err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatalf("cannot connect to database: %s", err)
	}

	fmt.Println("connected to database")

	return db
}

func ParseRowFromDB(query string, conn *sql.DB) map[string]interface{} {
	rows, err := conn.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var data map[string]interface{}

	columns, err := rows.Columns()
	if err != nil {
		log.Fatal(err)
	}

	columnTypes, err := rows.ColumnTypes()
	if err != nil {
		log.Fatal(err)
	}

	values := MakeValuesFromColumns(columnTypes)

	for rows.Next() {
		if err := rows.Scan(values...); err != nil {
			log.Fatal(err)
		}
	}

	for k, v := range columnTypes {
		empVal := *values[k].(*interface{})
		if empVal == nil {
			switch v.DatabaseTypeName() {
			case "UUID", "VARCHAR", "CHARACTER", "CHAR", "TEXT", "TIMESTAMP", "DATE", "TIME", "INTERVAL":
				values[k] = ""
			case "BOOLEAN":
				values[k] = false
			case "SMALLINT", "INTEGER", "BIGINT", "NUMERIC", "INT8", "INT4":
				values[k] = 0
			case "REAL", "DOUBLE PRECISION":
				values[k] = 0.0
			default:
				fmt.Printf("Type: unknown, Value: %v\n", v.DatabaseTypeName())
			}
		} else {
			switch v.DatabaseTypeName() {
			case "UUID":
				a := empVal.([]byte)
				values[k], _ = uuid.Parse(string(a))
			case "NUMERIC":
				a := empVal.([]byte)
				result := 0
				for _, b := range a {
					result = result*10 + int(b-'0')
				}
				values[k] = result
			}
		}
	}

	data = ScanRow(columns, values)

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	return data
}

func ParseRowsFromDB(query string, conn *sql.DB) []map[string]interface{} {
	rows, err := conn.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var data []map[string]interface{}

	columns, err := rows.Columns()
	if err != nil {
		log.Fatal(err)
	}

	columnTypes, err := rows.ColumnTypes()
	if err != nil {
		log.Fatal(err)
	}

	values := MakeValuesFromColumns(columnTypes)

	for rows.Next() {
		if err := rows.Scan(values...); err != nil {
			log.Fatal(err)
		}
		for k, v := range columnTypes {
			empVal := *values[k].(*interface{})
			if empVal == nil {
				switch v.DatabaseTypeName() {
				case "VARCHAR", "CHARACTER", "CHAR", "TEXT", "TIMESTAMP", "DATE", "TIME", "INTERVAL":
					values[k] = ""
				case "BOOLEAN":
					values[k] = false
				case "SMALLINT", "INTEGER", "BIGINT", "INT8", "INT4":
					values[k] = 0
				case "REAL", "DOUBLE PRECISION":
					values[k] = 0.0
				case "UUID":
					values[k] = ""
				case "NUMERIC":
					values[k] = 0
				default:
					fmt.Printf("Type: unknown, Value: %v\n", v.DatabaseTypeName())
				}
			} else {
				switch v.DatabaseTypeName() {
				case "UUID":
					a := empVal.([]byte)
					values[k], _ = uuid.Parse(string(a))
				case "NUMERIC":
					a := empVal.([]byte)
					result := 0
					for _, b := range a {
						result = result*10 + int(b-'0')
					}
					values[k] = result
				}
			}
		}
		sc := ScanRow(columns, values)
		data = append(data, sc)
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	return data
}
