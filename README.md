# Тулза для генерации джсонки из таблиц(-ы) базы данных

## Пример:

Прдеставим что мы имеем 3 таблицы в базе, и нам нужно их не просто соединить в одну структуру джсонку
а еще и переобразовать название наших ключей, которые являются в базе названиями столбцов
из вида "bonus_card_number" в "bonusCardNumber"

Таблицы в базе:
1-я таблица имеет такой вид(в примере указаны строки(данные) с базы):
```json
{
    "bonus_card_number": "9966026595412420",
    "cash_number": "323232",
    "code_company": 5000351,
    "code_shop": 240,
    "create_time": "2024-01-12T08:21:02.223727Z",
    "description": "",
    "document_type": "RC",
    "end_time": "2024-01-12T08:28:58.895858Z",
    "file_url": "",
    "g_user_id": 1,
    "goods_count": 1,
    "receipt_hash": "240/313131/565/1705047662",
    "receipt_id": "06bf83e5-a0aa-4e3f-bdbd-8a1f4fd48881",
    "receipt_number": "565",
    "return_receipt_number": "541",
    "return_receipt_time": "2024-01-11T15:05:06.994Z",
    "status": 1,
    "temp_receipt_id": 511,
    "total_amount": 1295,
    "total_amount_vat": 1450,
    "user_full_name": "Кассир 1"
}
```
2-я таблица:
```json
{
    "amount": 1295,
    "amount_vat": 1450,
    "bar_code": "2124823008864",
    "earned_bonus": 26,
    "g_goods_id": 167289,
    "g_return_reason_id": 2943,
    "g_unit_id": 7,
    "goods_name": "ТОРТ КАРМЕН",
    "order_number": 1,
    "paid_bonus": 0,
    "price": 2589,
    "price_dealer": 261442857142,
    "price_vat": 2900,
    "quantity": 2545,
    "reason_code": "99",
    "receipt_detail_id": "099c7d7b-e987-4427-9f91-cf008c707c17",
    "receipt_id": "06bf83e5-a0aa-4e3f-bdbd-8a1f4fd48881",
    "sale_amount": 0,
    "sale_bar_code": "",
    "sale_code": "",
    "temp_receipt_id": 511,
    "vat": 37400
}
```
3-я таблица:
```json
{
    "amount": 1450,
    "amount_change": 0,
    "cert_bar_code": "",
    "cert_code": "",
    "cert_nominal": 0,
    "cert_type": "",
    "document_type": "R",
    "g_payment_card_type_id": 0,
    "payment_kind_id": "f6828c42-a071-4809-99b2-bf3b71e1fda7",
    "payment_type": "NC",
    "receipt_id": "06bf83e5-a0aa-4e3f-bdbd-8a1f4fd48881",
    "status": 1,
    "temp_payment_kind_id": 317,
    "temp_receipt_id": 511
}
```
С помощью данной тулзы мы получаем то что нужно:
```json
{
    "bonusCardNumber": "9966026595412420",
    "cashNumber": "323232",
    "codeCompany": 5000351,
    "codeShop": 240,
    "createTime": "2024-01-12T08:21:02.223727Z",
    "description": "",
    "documentType": "RC",
    "endTime": "2024-01-12T08:28:58.895858Z",
    "fileUrl": "",
    "gUserId": 1,
    "goodsCount": 1,
    "paymentKind": [
            {
                "amount": 1450,
                "amountChange": 0,
                "certBarCode": "",
                "certCode": "",
                "certNominal": 0,
                "certType": "",
                "documentType": "R",
                "gPaymentCardTypeId": 0,
                "paymentKindId": "f6828c42-a071-4809-99b2-bf3b71e1fda7",
                "paymentType": "NC",
                "receiptId": "06bf83e5-a0aa-4e3f-bdbd-8a1f4fd48881",
                "status": 1,
                "tempPaymentKindId": 317,
                "tempReceiptId": 511
            }
        ],
    "receiptDetail": [
            {
                "amount": 1295,
                "amountVat": 1450,
                "barCode": "2124823008864",
                "earnedBonus": 26,
                "gGoodsId": 167289,
                "gReturnReasonId": 2943,
                "gUnitId": 7,
                "goodsName": "ТОРТ КАРМЕН",
                "orderNumber": 1,
                "paidBonus": 0,
                "price": 2589,
                "priceDealer": 261442857142,
                "priceVat": 2900,
                "quantity": 2545,
                "reasonCode": "99",
                "receiptDetailId": "099c7d7b-e987-4427-9f91-cf008c707c17",
                "receiptId": "06bf83e5-a0aa-4e3f-bdbd-8a1f4fd48881",
                "saleAmount": 0,
                "saleBarCode": "",
                "saleCode": "",
                "tempReceiptId": 511,
                "vat": 37400
            }
        ],
    "receiptHash": "240/313131/565/1705047662",
    "receiptId": "06bf83e5-a0aa-4e3f-bdbd-8a1f4fd48881",
    "receiptNumber": "565",
    "returnReceiptNumber": "541",
    "returnReceiptTime": "2024-01-11T15:05:06.994Z",
    "status": 1,
    "tempReceiptId": 511,
    "totalAmount": 1295,
    "totalAmountVat": 1450,
    "userFullName": "Кассир 1"
}
```