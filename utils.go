package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"strings"
)

func MakeValuesFromColumns(columns []*sql.ColumnType) []interface{} {
	values := make([]interface{}, len(columns))
	for i := range values {
		var value interface{}
		values[i] = &value
	}

	return values
}

func ScanRow(columns []string, values []interface{}) map[string]interface{} {
	rowMap := make(map[string]interface{})
	for i, colName := range columns {
		rowMap[colName] = values[i]
	}

	return rowMap
}

func Marshal(data interface{}) []byte {
	jsonData, err := json.MarshalIndent(data, " ", " ")
	if err != nil {
		log.Fatal(err)
	}

	return jsonData
}

func MergeMaps(m1 map[string]interface{}, m2, m3 []map[string]interface{}) map[string]interface{} {
	m := make(map[string]interface{})
	for k, v := range m1 {
		m[k] = v
	}
	m["receiptDetail"] = m2
	m["paymentKind"] = m3

	for k, v := range m1 {
		m[k] = v
	}
	m["receiptDetail"] = m2
	m["paymentKind"] = m3

	return m
}

func JsonReplacer(m map[string]interface{}) map[string]interface{} {
	for k, v := range m {
		if mps, ok := v.([]map[string]interface{}); ok {
			for _, mp := range mps {
				for mpk, mpv := range mp {
					if strings.Contains(mpk, "_") {
						parts := strings.Split(mpk, "_")
						for i := 1; i < len(parts); i++ {
							parts[i] = strings.Title(parts[i])
						}
						ch := strings.Join(parts, "")
						mp[ch] = mpv
						delete(mp, mpk)
					}
				}
			}
		} else {
			if strings.Contains(k, "_") {
				parts := strings.Split(k, "_")
				for i := 1; i < len(parts); i++ {
					parts[i] = strings.Title(parts[i])
				}
				ch := strings.Join(parts, "")
				m[ch] = v
				delete(m, k)
			}
		}
	}

	return m
}
